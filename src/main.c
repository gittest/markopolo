#include <stdio.h>
#include <string.h>


#define BUY	"buy_one"
#define COPY	"copy_paste"

#define M_C	21
#define M_G	1000

int
main (void)
{
	int travel;	/* Number of travels */
	int shop;	/* Number of shops in A travel */
	char gift[M_C];
	int resault[M_G];	/* Resault for print */
	int sum;
	int i, d;	/* Use for loops */


	scanf ("%d", &travel);	/* Get number of Travel */

	for (i = 0; i < travel; i++)
	{
		sum = 0;	/* INIT sum */

		scanf ("%d", &shop);	/* Get number of shops in A travel*/
		getchar ();		/* fix for fgets */

		for (d = 0; d < shop; d++)
		{

			gets (gift);
			
			if (sum == 0)
			{
				if (strstr(gift, BUY) != NULL)
					sum++;
			}

			else
			{
				if (strstr(gift, COPY) != NULL)
					sum *= 2;
				else if (strstr(gift, BUY) != NULL)
					sum++;
			}

		}

		resault[i] = sum;
	}

	for (i = 0; i < travel; i++)
		printf ("%d\n", resault[i]);

	return 0;
}



